﻿namespace WebClient.SQL
{
    public class Script
    {
        public const string sqlCreateTableHumanInfo = @"

CREATE SEQUENCE Customer_ID_SEQ
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 999999999
    CACHE 1
    ;

CREATE TABLE CustomerInfoProject
(
    ID bigint NOT NULL DEFAULT NEXTVAL('Customer_ID_SEQ'),
    FirstName character varying NOT NULL,
    LastName character varying NOT NULL,
    CONSTRAINT CustomerInformation_pkey PRIMARY KEY(ID)
)

TABLESPACE pg_default;
";


        public const string GetCustomersQuery = @"
            SELECT
                Id,
                Firstname,
                Lastname
            FROM
                CustomerInfoProject
        ";
    }

    
}

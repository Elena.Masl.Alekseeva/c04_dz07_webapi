﻿using Newtonsoft.Json;
using Npgsql;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebClient
{
    static class Program
    {
        static string FromConsol = string.Empty;
        
        static async Task Main(string[] args)
        {

            CreateDB();

            WriteProgramInfo();
            FromConsol = Console.ReadLine();

            while (FromConsol != "3")
            {
                if (FromConsol == "1")
                {
                    CustomerCreateRequest customerCreate = RandomCustomer();
                    string jsonString = JsonConvert.SerializeObject(customerCreate);
                    StringContent httpContent = new StringContent(jsonString, System.Text.Encoding.UTF8, "application/json");

                    var client = new HttpClient();
                    var response = await client.PostAsync("https://localhost:5001/customers/Add", httpContent);
                    string id = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();

                    Console.WriteLine("Был добавлен пользователь " + customerCreate.Firstname + " " + customerCreate.Lastname + " " + "с ID = " + id);
                }
                else
                    if (FromConsol == "2")
                {
                    Console.WriteLine("Введите id клиента");
                    string idString = Console.ReadLine();

                    var client = new HttpClient();
                    string str = "https://localhost:5001/customers/" + idString;
                    var response = await client.GetAsync(str);

                    if (response.Content.Headers.ContentType?.MediaType == "application/json")
                    {
                        string ss = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                        //Console.WriteLine(ss);
                        ss = ss.Replace("[", string.Empty).Replace("]", string.Empty);
                        Customer re = JsonConvert.DeserializeObject<Customer>(ss);

                        if (re == null)
                            Console.WriteLine("Такой клиент не найден.");
                        else
                        {
                            ss = ss.Replace("{", string.Empty).Replace("}", string.Empty);
                            Console.WriteLine(ss);
                        }
                    }

                }
                Console.WriteLine();
                WriteProgramInfo();
                FromConsol = Console.ReadLine();

            }
        }

        /// <summary>
        /// Генерация случайного пользователя
        /// </summary>
        /// <returns></returns>
        private static CustomerCreateRequest RandomCustomer()
        {
            Random rnd = new Random();
            int randInt = rnd.Next(1000, 20000);
            CustomerCreateRequest customerCreate = new CustomerCreateRequest(String.Format("FirstName{0}", randInt), String.Format("LastName{0}", randInt));
            return customerCreate;
        }

        /// <summary>
        /// Вывод инфо
        /// </summary>
        static void WriteProgramInfo()
        {
            Console.WriteLine("Введите - 1, если хотите соз-дать клиента,");
            Console.WriteLine("Введите - 2, если хотите запросить данные пользователя с сервера,");
            Console.WriteLine("Введите - 3, если хотите выйти из программы.");
        }

        static void CreateDB()
        {
            try
            {

                const string connectString = "Host=localhost;Port=5432;Username=postgres;Password=postgres;Database=postgres";
                using NpgsqlConnection connection = new NpgsqlConnection(connectString);
                connection.Open();

                using (Npgsql.NpgsqlCommand command = new Npgsql.NpgsqlCommand(cmdText: WebClient.SQL.Script.sqlCreateTableHumanInfo, connection: connection))
                {
                    command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            { }
        }
    }
}
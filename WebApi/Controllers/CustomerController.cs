using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
using WebApi.Repositories.Implement;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        CustomerRepositories repo;

        public CustomerController()
            : base()
        {
            repo = new CustomerRepositories();
        }

        /// <summary>
        /// Возвращает пользователя по id
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        [HttpGet("{id}")]   
        public async Task<ActionResult<Customer>> GetCustomerAsync([FromRoute] Int64 id)
        {
            try
            {
                var cust = await repo.GetByID(id); ;
                
                if (cust != null && cust.Count > 0)
                    return Ok(cust);
                else 
                    if (cust != null && cust.Count == 0)
                        return NotFound(cust);
                else return BadRequest();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
        
        /// <summary>
        /// Добавляет пользователя
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        [HttpPost("Add")]
        public async Task<ActionResult<long>> CreateCustomerAsync([FromBody] Customer customer)
        {
            try
            {
                return await repo.CreateCustomer(customer);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }
        }
    }
}
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Npgsql;
using WebApi.SQL;

namespace WebApi
{
    public static class Program
    {
        
        


        public static async Task Main(string[] args)
        {

            


                await CreateHostBuilder(args).Build().RunAsync();
            /*
            using (Npgsql.NpgsqlConnection connectionInProj = new Npgsql.NpgsqlConnection(connectString))
            {
                connectionInProj.Open();
                
                using (Npgsql.NpgsqlCommand command = new Npgsql.NpgsqlCommand(cmdText: sqlCreateTableHumanInfo, connection: connectionInProj))
                {
                    command.ExecuteNonQuery();
                }
            }*/
        }

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });

        /// <summary>
        /// ������ � ������� �������������
        /// </summary>
        /// <param name="connectionInProj">��������</param>
        /// <param name="FirstName"></param>
        /// <param name="LastName"></param>
        /// <param name="MidName"></param>
        /// <param name="PhoneNumber"></param>
        /// <param name="Email"></param>
        /// <returns></returns>
        private static Int64 InsertHumanRecord(Npgsql.NpgsqlConnection connectionInProj, string FirstName, string LastName, string MidName, string PhoneNumber, string Email)
        {/*
            Int64 idHumanAdded = -1;
            using (Npgsql.NpgsqlCommand command = new Npgsql.NpgsqlCommand(cmdText: SQLScripts.sqlInsertHuman, connection: connectionInProj))
            {
                Npgsql.NpgsqlParameterCollection parameters = command.Parameters;
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "FirstName", value: FirstName));
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "LastName", value: LastName));
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "MidName", value: MidName));
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "PhoneNumber", value: PhoneNumber));
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "Email", value: Email));

                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "BirthDate", value: DateTime.Now));
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "IsDeleted", value: Convert.ToBoolean(0)));
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "DeleteDate", value: DateTime.MinValue));

                idHumanAdded = Convert.ToInt64(command.ExecuteScalar());
            }

            return idHumanAdded;
            */
            return 0;
        }

        /// <summary>
        /// ����� ������ �� �������
        /// </summary>
        /// <param name="connectionInProj">��������</param>
        /// <param name="tableName">��� �������</param>
        private static void OutputTable(Npgsql.NpgsqlConnection connectionInProj, string tableName)
        {
            Console.WriteLine("{0} data : ", tableName);
            string sqlStr = string.Format("SELECT * FROM {0}", tableName);
            using (Npgsql.NpgsqlCommand command = new Npgsql.NpgsqlCommand(sqlStr, connection: connectionInProj))
            {
                Npgsql.NpgsqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {

                    string res = string.Empty;
                    for (int i = 0; i < reader.FieldCount; i++)
                        res += "\t" + reader.GetName(i).ToString();

                    Console.WriteLine(res);
                    res = string.Empty;
                    while (reader.Read()) // ��������� ��������� ������
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            string dp = (!string.IsNullOrEmpty(reader.GetValue(i).ToString()) ? reader.GetValue(i).ToString() : "-");
                            res += "\t" + dp;
                        }
                        Console.WriteLine(res);
                        res = string.Empty;
                    }
                }
                reader.Close();
            }
        }

    }
}
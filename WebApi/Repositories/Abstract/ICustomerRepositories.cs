﻿using System.Collections.Generic;
using WebApi.Models;

namespace WebApi.Repositories.Abstract
{
    public interface ICustomerRepositories
    {
        /// <summary>
        /// Возвращает всех пользователей
        /// </summary>
        /// <returns></returns>
        List<Customer> GetAll();
        
        /// <summary>
        /// Добавить пользователя
        /// </summary>
        /// <param name="category">Категория</param>
        /// <returns></returns>
        int CreateCategory(Customer category);
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Models;
using WebApi.SQL;

namespace WebApi.Repositories.Implement
{
    public class CustomerRepositories
    {

        const string connectString = "Host=localhost;Port=5432;Username=postgres;Password=postgres;Database=postgres";

        /// <summary>
        /// Возвращает пользователя по id
        /// </summary>
        /// <returns></returns>
        public async Task<List<Customer>> GetByID(Int64 id)
        {
            try
            {
                List<Customer> resultList = new List<Customer>();

                using NpgsqlConnection connection = new NpgsqlConnection(connectString);
                connection.Open();

                string sql = string.Format(Script.SelectCastByID, id);
                using (Npgsql.NpgsqlCommand command = new Npgsql.NpgsqlCommand(cmdText: sql, connection: connection))
                {

                    Npgsql.NpgsqlDataReader reader = await command.ExecuteReaderAsync();
                    if (reader.HasRows)
                    {
                        string res = string.Empty;
                        while (reader.Read()) // построчно считываем данные
                        {
                            string strId = (!string.IsNullOrEmpty(reader.GetValue(0).ToString()) ? reader.GetValue(0).ToString() : null);
                            Int64 idForInit = Convert.ToInt64(strId);
                            string strFirstName = (!string.IsNullOrEmpty(reader.GetValue(1).ToString()) ? reader.GetValue(1).ToString() : null);
                            string strLastName = (!string.IsNullOrEmpty(reader.GetValue(2).ToString()) ? reader.GetValue(2).ToString() : null);

                            Customer cast = new Customer(idForInit, strFirstName, strLastName);
                            
                            resultList.Add(cast);
                        }
                    }
                    reader.Close();
                    
                    return resultList;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Добавить пользователя
        /// </summary>
        /// <param name="category">Пользователь</param>
        /// <returns></returns>
        public async Task<Int64> CreateCustomer(Customer category)
        {
            using NpgsqlConnection connection = new NpgsqlConnection(connectString);
            connection.Open();

            Int64 idHumanAdded = -1;
            using (Npgsql.NpgsqlCommand command = new Npgsql.NpgsqlCommand(cmdText: Script.InsertCustomersQuery, connection: connection))
            {
                Npgsql.NpgsqlParameterCollection parameters = command.Parameters;
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "Firstname", value: category.Firstname));
                parameters.Add(value: new Npgsql.NpgsqlParameter(parameterName: "Lastname", value: category.Lastname));


                var res = await command.ExecuteScalarAsync();
                idHumanAdded = Convert.ToInt64(res);
            }

            return idHumanAdded;
        }
    }
}

﻿namespace WebApi.SQL
{
    public class Script
    {

        public const string GetCustomersQuery = @"
            SELECT
                Id,
                Firstname,
                Lastname
            FROM
                CustomerInfoProject
        ";

        /// <summary>
        /// Строка для скрипта создания таблицы объявлений
        /// </summary>
        public const string InsertCustomersQuery = @"

INSERT INTO CustomerInfoProject (    
    FirstName,
    LastName) 
VALUES(    
    :FirstName,
    :LastName) RETURNING id; 

";

        /// <summary>
        /// Строка для скрипта выбрать таблицу 
        /// </summary>
        public const string SelectCastByID = @"

SELECT * FROM public.CustomerInfoProject WHERE id={0}
";
    }

    //SELECT * FROM public.CustomerInfoProject WHERE id={0} 
}
